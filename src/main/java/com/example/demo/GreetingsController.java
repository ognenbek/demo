package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingsController {

  @GetMapping("/demo")
  public String demo(@RequestParam(name = "name", required = false, defaultValue = "Andriko") String name, Model model) {
    model.addAttribute("name", name);
    return "demo";
  }
}
